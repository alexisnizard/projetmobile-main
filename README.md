#  InterimExpress : Application mobile pour la gestion de candidatures et d’offres en interim

### Introduction

Ce projet consiste à développer une application mobile pour la gestion des offres et des candidatures pour le travail en intérim. L'objectif principal est de fournir une plateforme efficace et intuitive pour les candidats, employeurs et agences, facilitant ainsi la recherche d'offres d'emploi temporaire et la gestion de candidatures.

### Technologies
L'application est développée en utilisant le langage de programmation Kotlin, avec Android Studio comme environnement de développement.
En ce qui concerne la gestion des données, nous avons opté pour Firebase Cloud Firestore.

### Exemples d'affichages
Ci-dessous, vous trouverez des exemples d'affichages de l'application  :

<p align="center">
  <img src="exemple1.png" alt="Exemple d'exécution du programme" width="300">
  <img src="exemple2.png" alt="Exemple d'exécution du programme" width="300">
  <img src="exemple3.png" width="300">
</p>
